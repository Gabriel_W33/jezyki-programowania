const a = document.querySelector("#a")
const b = document.querySelector("#b")
const c = document.querySelector("#c")
c.value = 0
a.addEventListener('change', () => { updateValues(a, b, c) })
b.addEventListener('change', () => { updateValues(a, b, c) })

const updateValues = (x, y, z) => { z.value = Number(x.value) + Number(y.value) }
