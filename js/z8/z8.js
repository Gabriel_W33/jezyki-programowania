var fig=[
    {name:"Punkt",x:1,y:3},
    {name:"Kolo",x:2,y:4,r:4,color:"green"},
    {name:"Prostokat",x:5,y:6,a:2,b:4,color:"red"},
    {name:"Wielokat",x:[5,3,5,6,7],y:[5,6,7,8,10],color:"pink"},
];
    

const drawCanvas = (tab, id) => {
    const canvas = document.getElementById(id)
    canvas.setAttribute("width", '1000', "height", '1000')
    const ctx = canvas.getContext('2d')
    var scale = 20
    
    tab.forEach(fig => {


        if(fig.name === "Punkt") {
            ctx.fillRect(fig.x, fig.y, 3, 3)
        
        
        } else if (fig.name === "Kolo") { 
            ctx.beginPath();
            ctx.arc(fig.x*scale , fig.y*scale , fig.r*scale , 0, 2 * Math.PI);
            ctx.fillStyle = fig.color
            ctx.strokeStyle = fig.color
            ctx.fill()
            ctx.stroke() 

        } else if (fig.name === "Prostokat") {
            ctx.fillStyle = fig.color
            ctx.fillRect(fig.x *scale, fig.y *scale, fig.a *scale, fig.b * scale)

            
        } else if (fig.name === "Wielokat") {
            
            ctx.beginPath()
            ctx.moveTo(fig.x[0] * scale, fig.y[0] * scale);
            
            for(let i = 1; i <= fig.x.length; i++) {
                ctx.lineTo(fig.x[i] * scale, fig.y[i] * scale);
            }
            ctx.closePath();
            ctx.fillStyle = fig.color
            ctx.strokeStyle = fig.color
            ctx.fill();
            ctx.stroke() 
        } else
            return null
    })  
}

drawCanvas(fig, "myCanvas")

    
const conPun = (x, y, p) => {
    if((x === p.x && y === p.y)) {
        return "jest w miejscu punktu"
    }else return "niema na punkcie "   
}
const conKol = (x, y,k) => {
    if(Math.pow((x - k.x), 2) + Math.pow((y - k.y), 2) < Math.pow(k.r, 2)) {
        return "jest w kole"
    }    else "miema w kole" 
}
const conPro = (x, y,pr) => {
    if((pr.x < x && x < pr.x + pr.a) && (pr.y < y && y < pr.y + pr.b)) {
        return "jest W prostokacie"
    }else return "niema w prostokacie"
}



document.writeln(conPun(1,3,fig[0]) + "<br>")
document.writeln(conKol(1,3,fig[1])+ "<br>")
document.writeln(conPro(1,3,fig[2])+ "<br>")