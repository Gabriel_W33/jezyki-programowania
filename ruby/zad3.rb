require 'net/http'

pages = %w( http://kicia.ift.uni.wroc.pl
		https://www.wp.pl
            https://www.google.com
            http://www.ruby-lang.org )

for page in pages
		response = Net::HTTP.get_response(URI(page))
		name = page.sub /(https|http):\/\//,''
		File.write('stronki/'+name, response.body)
end

